<?php
namespace Aureatelabs\EstimatedDelivery\Model;

use Aureatelabs\EstimatedDelivery\Helper\Data;

class Products
{
    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * Products constructor.
     * @param Data $helper
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     */
    public function __construct(
        Data $helper,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    ) {
        $this->helper = $helper;
        $this->productRepository = $productRepository;
    }

    /**
     * @param $sku
     * @return array
     * @throws \Exception
     */
    public function getEstimatedDeliveryDate($sku)
    {
        $product = $this->loadProductBySku($sku);
        $deliveryDate = $this->helper->getEstimatedDeliveryDate($product);

        return $deliveryDate;
    }

    /**
     * @param $sku
     * @return \Magento\Catalog\Api\Data\ProductInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function loadProductBySku($sku)
    {
        return $this->productRepository->get($sku);
    }
}
