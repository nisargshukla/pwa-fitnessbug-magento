<?php
namespace Aureatelabs\EstimatedDelivery\Helper;

class Data extends \Wyomind\EstimatedDeliveryDate\Helper\Data
{
    /**
     * @param $product
     * @return string|null
     * @throws \Exception
     */
    public function getEstimatedDeliveryDate($product)
    {
        $deliveryDate[] = $this->renderMessagePwa($product);

        return $deliveryDate;
    }

    /**
     * @param null $product
     * @param int $currentTime
     * @param int $storeId
     * @param string $context
     * @param int $additionalFrom
     * @param int $additionalTo
     * @param int $qty
     * @param bool $shippingMethod
     * @return null|string
     * @throws \Exception
     */
    public function renderMessagePwa($product = null, $currentTime = -1, $storeId = 0, $context = "product", $additionalFrom = 0, $additionalTo = 0, $qty = 1, $shippingMethod = false)
    {
        try {
            if ($this->framework->getStoreConfig("estimateddeliverydate/common/enabled", $storeId)) {
                $html = null;
                $label = null;
                $isPreview = $context == "preview";
                if (!$product) {
                    return;
                }
                $stockStatus = $this->getStockStatus($product, $storeId, $qty);
                $partsToGenerate = [];
                if (($isPreview || $context == "product" || $context == null) && $this->getProductSettings($product, "use_message_for_product", $stockStatus, $storeId)) {
                    $useMessageForProduct = $this->getProductSettings($product, "use_message_for_product", $stockStatus, $storeId);
                    if ($useMessageForProduct) {
                        $partsToGenerate[] = "product";
                    }
                }
                $leadTimes = false;
                if (!empty($partsToGenerate)) {
                    $leadTimes = $this->getLeadTimes($product, $currentTime, $storeId, $additionalFrom, $additionalTo, $qty, true, $shippingMethod);
                }
                if ($leadTimes == false) {
                    $html .= self::renderTrace();
                    return $html;
                }

                $messageForeachCartItem = $this->getProductSettings($product, "message_foreach_cart_item", $stockStatus, $storeId);
                $parseMessageForCartItem = $this->parseMessage($messageForeachCartItem, $leadTimes);
                if (in_array("product", $partsToGenerate)) {
                    $this->countdownFormat = $this->framework->getStoreConfig("estimateddeliverydate/countdown/format", $storeId);
                    $this->countdownType = $this->framework->getStoreConfig("estimateddeliverydate/countdown/type", $storeId);
                    $this->countdownRealTime = $this->framework->getStoreConfig("estimateddeliverydate/countdown/realtime", $storeId);
                    $messageForProduct = $this->getProductSettings($product, "message_for_product", $stockStatus, $storeId);
                    $parseMessageForProduct = $this->parseMessagePwa($messageForProduct, $leadTimes, true, $parseMessageForCartItem);
                    $html = $parseMessageForProduct;
                }

                return $html;
            }
        } catch (\Exception $e) {
            if (true) {
                throw new \Exception($e->getMessage());
            }
        }
    }

    /**
     * @param null $message
     * @param array $leadtimes
     * @param bool $countdownAllowed
     * @return mixed|null
     */
    protected function parseMessagePwa($message = null, $leadtimes = array(), $countdownAllowed = false, $cartMessage = null)
    {
        if ($leadtimes['to'] <= $leadtimes['from']) {
            $leadtimes['to'] = $leadtimes['from'];
            if ($this->framework->getStoreConfig("estimateddeliverydate/common/add_one_day_to", $this->_storeManager->getStore()->getId())) {
                $leadtimes['to'] += 1;
            }
        }
        $date['from'] = $this->dateTranslate($this->_coreDateTime->date($this->_dateFormat, $this->_gmtCurrentTime + 86400 * $leadtimes['from']));
        $date['to'] = $this->dateTranslate($this->_coreDateTime->date($this->_dateFormat, $this->_gmtCurrentTime + 86400 * $leadtimes['to']));
        $placeHolder = null;
        if ($countdownAllowed) {
            $cutOff = explode(',', $this->_lastShippingTime);
            $cutOffTime = $this->_todayMidnight + $cutOff[0] * 3600 + $cutOff[1] * 60 + $cutOff[2] + 86400 * $this->delay;
            $countdown = $cutOffTime - $this->_currentTime;
            $placeHolder = abs($countdown);
        }

        $message = [
            "delivery_date" => $date["from"],
            "countdown" => $placeHolder,
            "message" => $message,
            "cartMessage" => $cartMessage
        ];

        return $message;
    }

}