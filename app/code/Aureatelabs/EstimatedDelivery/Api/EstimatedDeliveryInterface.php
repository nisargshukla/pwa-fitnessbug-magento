<?php
namespace Aureatelabs\EstimatedDelivery\Api;

interface EstimatedDeliveryInterface
{
    /**
     * Get Estimated date from SKU
     *
     * @param string $sku
     * @return mixed
     */
    public function getEstimatedDeliveryDate($sku);
}