<?php
namespace Aureatelabs\KlarnaOrderRemove\Model;

class Order implements \Aureatelabs\KlarnaOrderRemove\Api\OrderInterface
{
    protected $payCol;
    
    protected $orderRes;

    public function __construct(
        \Magento\Sales\Model\ResourceModel\Order\Payment\CollectionFactory $payCol,
        \Magento\Sales\Model\ResourceModel\Order $orderRes
    ) {
        $this->payCol = $payCol;
        $this->orderRes = $orderRes;
    }

    /**
     * @param string $orderId 
     * @return string 
     */
    public function getMagentoOrderId($orderId) {
        $col = $this->payCol->create();
        $col->addFieldToFilter('last_trans_id', $orderId);

        foreach ($col as $key => $value) {
            $orderId = $value->getParentId();
            break;
        }

        $sql = 'SELECT increment_id FROM sales_order WHERE entity_id = :entity_id';
        $orderInc = $this->orderRes->getConnection()->fetchOne($sql, [
            'entity_id' => $orderId
        ]);

        return $orderInc;
    }
}
