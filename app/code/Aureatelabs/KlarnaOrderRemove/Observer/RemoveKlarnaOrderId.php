<?php

namespace Aureatelabs\KlarnaOrderRemove\Observer;

use Magento\Framework\Event\ObserverInterface;

class RemoveKlarnaOrderId implements ObserverInterface
{  
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $transport = $observer->getEvent()->getTransport();
        if(strpos($transport['payment_html'], "klarna.com") !== false){
        	$logo_url = 'https://cdn.klarna.com/1.0/shared/image/generic/logo/en_us/basic/logo_black.png?width=300';
		    $transport['payment_html'] = '<p><img width="150" src="'.$logo_url.'" alt="klarna"/></p>';
		}
    }
}