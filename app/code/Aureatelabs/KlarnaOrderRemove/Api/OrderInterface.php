<?php
namespace Aureatelabs\KlarnaOrderRemove\Api;

interface OrderInterface
{
    /**
     * @param string $orderId 
     * @return string
     */
    public function getMagentoOrderId($orderId);
}
