<?php
namespace Aureatelabs\ProductReviewImages\Model\ResourceModel;

use Magento\Framework\Model\AbstractModel;
use Magento\Review\Model\ResourceModel\Rating;
use Magento\Framework\Api\Data\ImageContentInterface;
use Magento\Framework\Api\ImageProcessor;

class Review extends \Magento\Review\Model\ResourceModel\Review
{
    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    protected $json;

    /**
     * @var \Magento\Framework\Api\ImageProcessor
     */
    protected $imageProcessor;

    /**
     * @var string
     */
    protected $destinationFolder = 'review_images';

    /**
     * @var ImageContentInterface
     */
    private $imageContentInterface;

    /**
     * Review constructor.
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Review\Model\RatingFactory $ratingFactory
     * @param Rating\Option $ratingOptions
     * @param ImageProcessor $imageProcessor
     * @param ImageContentInterface $imageContentInterface
     * @param \Magento\Framework\Serialize\Serializer\Json $json
     * @param null $connectionName
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Review\Model\RatingFactory $ratingFactory,
        Rating\Option $ratingOptions,
        ImageProcessor $imageProcessor,
        ImageContentInterface $imageContentInterface,
        \Magento\Framework\Serialize\Serializer\Json $json,
        $connectionName = null
    ) {
        $this->json = $json;
        $this->imageProcessor = $imageProcessor;
        $this->imageContentInterface = $imageContentInterface;
        parent::__construct($context, $date, $storeManager, $ratingFactory, $ratingOptions, $connectionName);
    }

    /**
     * @param AbstractModel $object
     * @return $this|\Magento\Review\Model\ResourceModel\Review
     * @throws \Magento\Framework\Exception\InputException
     */
    protected function _afterSave(AbstractModel $object)
    {
        $connection = $this->getConnection();
        $result = [];
        $images = $object->getImages();
        if (!empty($images) && !is_string($object->getImages())) {
            foreach ($images as $image) {
                $image = $this->json->unserialize($image);
                $this->imageContentInterface->setBase64EncodedData($image['base64_encoded_data']);
                $this->imageContentInterface->setType($image['type']);
                $this->imageContentInterface->setName($image['name']);

                $result[] = $this->processImages($this->imageContentInterface);
            }
        }

        $detail = [
            'title' => $object->getTitle(),
            'detail' => $object->getDetail(),
            'nickname' => $object->getNickname(),
            'images' => !empty($result) && !is_string($object->getImages()) ? $this->json->serialize($result) : $object->getImages()
        ];

        $select = $connection->select()->from($this->_reviewDetailTable, 'detail_id')->where('review_id = :review_id');
        $detailId = $connection->fetchOne($select, [':review_id' => $object->getId()]);

        if ($detailId) {
            $condition = ["detail_id = ?" => $detailId];
            $connection->update($this->_reviewDetailTable, $detail, $condition);
        } else {
            $detail['store_id'] = $object->getStoreId();
            $detail['customer_id'] = $object->getCustomerId();
            $detail['review_id'] = $object->getId();
            $connection->insert($this->_reviewDetailTable, $detail);
        }

        /**
         * save stores
         */
        $stores = $object->getStores();
        if (!empty($stores)) {
            $condition = ['review_id = ?' => $object->getId()];
            $connection->delete($this->_reviewStoreTable, $condition);

            $insertedStoreIds = [];
            foreach ($stores as $storeId) {
                if (in_array($storeId, $insertedStoreIds)) {
                    continue;
                }

                $insertedStoreIds[] = $storeId;
                $storeInsert = ['store_id' => $storeId, 'review_id' => $object->getId()];
                $connection->insert($this->_reviewStoreTable, $storeInsert);
            }
        }

        // reaggregate ratings, that depend on this review
        $this->_aggregateRatings($this->_loadVotedRatingIds($object->getId()), $object->getEntityPkValue());

        return $this;
    }

    /**
     * @param ImageContentInterface $imageContent
     * @return string
     * @throws \Magento\Framework\Exception\InputException
     */
    public function processImages(ImageContentInterface $imageContent)
    {
        $filePath = $this->imageProcessor->processImageContent($this->destinationFolder, $imageContent);
        return $this->destinationFolder . $filePath;
    }
}
