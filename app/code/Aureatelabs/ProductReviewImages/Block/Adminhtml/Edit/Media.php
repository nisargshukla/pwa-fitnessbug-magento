<?php
namespace Aureatelabs\ProductReviewImages\Block\Adminhtml\Edit;

use Magento\Review\Model\ReviewFactory;
use Magento\Framework\Exception\LocalizedException;

class Media extends \Magento\Backend\Block\Template
{
    /**
     * @var ReviewFactory
     */
    private $reviewFactory;

    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    protected $json;

    /**
     * Media constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Serialize\Serializer\Json $json
     * @param ReviewFactory $reviewFactory
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Serialize\Serializer\Json $json,
        ReviewFactory $reviewFactory
    ) {
        $this->reviewFactory = $reviewFactory;
        $this->json = $json;
        $this->setTemplate("media.phtml");
        parent::__construct($context);
    }

    /**
     * @return array|mixed
     * @throws LocalizedException
     */
    public function getMediaCollection()
    {
        try {
            $review = $this->reviewFactory->create()->load($this->getRequest()->getParam('id'));
            $reviews = $review->getImages() !== null && $review->getImages() !== '' ? $this->json->unserialize($review->getImages()) : [];

        } catch (LocalizedException $exception) {
            throw new LocalizedException(__($exception->getMessage()));
        }

        return $reviews;
    }

    /**
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getReviewMediaUrl()
    {
        $reviewMediaDirectoryPath = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        return $reviewMediaDirectoryPath;
    }
}
