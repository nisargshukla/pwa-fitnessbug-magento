<?php

namespace Aureatelabs\ProductReviewImages\Plugin;

use Magento\ConfigurableProduct\Model\Product\Type\Configurable;

class AttributeDataPlugin
{
    /**
     * @var Configurable
     */
    protected $configurable;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resource;

    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $config;

    /**
     * AttributeDataPlugin constructor.
     * @param Configurable $configurable
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Magento\Eav\Model\Config $config
     */
    public function __construct(
        Configurable $configurable,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Eav\Model\Config $config
    ) {
        $this->configurable = $configurable;
        $this->resource = $resource;
        $this->config = $config;
    }

    /**
     * @param \Divante\VsbridgeIndexerCatalog\Model\Indexer\DataProvider\Product\AttributeData $subject
     * @param $result
     * @param $indexData
     * @param $storeId
     * @return mixed
     */
    public function afterAddData(
        \Divante\VsbridgeIndexerCatalog\Model\Indexer\DataProvider\Product\AttributeData $subject,
        $result,
        $indexData,
        $storeId
    ) {
        $allReviewData = $this->getReviewData();
        foreach ($result as &$product) {
            if ($product['id'] && isset($allReviewData[$product['id']])) {
                $product["no_of_review"] = count($allReviewData[$product['id']]);
                $sumRating = array_sum(array_column($allReviewData[$product['id']], "value"));
                $product['rating_count'] = $sumRating / $product["no_of_review"];
            }
        }
        return $result;
    }

    /**
     * @return array
     */
    public function getReviewData()
    {
        $allReviewData = [];
        $connection = $this->resource->getConnection();
        $review =  $this->resource->getTableName('review');
        $reviewDetailTable =  $this->resource->getTableName('review_detail');
        $ratingTable =  $this->resource->getTableName('rating_option_vote');
        $review = $connection->select()
            ->from(['review' => $review], ['review_id', 'status_id', 'entity_pk_value','created_at'])
            ->joinInner(
                ['review_detail' => $reviewDetailTable],
                'review.review_id = review_detail.review_id',
                ['detail_id', 'detail', 'title','nickname']
            )->joinInner(
                ['rating_table' => $ratingTable],
                'review.review_id = rating_table.review_id',
                ['value']
            )->where('review.status_id = 1')->order(['rating_table.value DESC']);
        $reviewData = $connection->fetchAll($review);
        foreach ($reviewData as $reviewDetail) {
            $allReviewData[$reviewDetail['entity_pk_value']][] = $reviewDetail;
        }
        return $allReviewData;
    }
}
