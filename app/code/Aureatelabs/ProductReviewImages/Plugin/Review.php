<?php
namespace Aureatelabs\ProductReviewImages\Plugin;

use Magento\Framework\App\ResourceConnection;

class Review
{
    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * Rates constructor.
     *
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(ResourceConnection $resourceConnection)
    {
        $this->resource = $resourceConnection;
    }

    public function aroundGetReviews(\Divante\VsbridgeIndexerReview\ResourceModel\Review $subject, callable $proceed, int $storeId = 1, array $reviewIds = [], int $fromId = 0, int $limit = 1000)
    {
        $select = $this->getConnection()->select()
            ->from(
                ['main_table' => $this->resource->getTableName('review')],
                [
                    'review_id',
                    'created_at',
                    'entity_pk_value',
                    'status_id',
                ]
            );

        $select->joinLeft(
            ['store' => $this->resource->getTableName('review_store')],
            'main_table.review_id = store.review_id'
        )->where('store.store_id = ?', $storeId);

        if (!empty($reviewIds)) {
            $select->where('main_table.review_id IN (?)', $reviewIds);
        }

        $select->where('entity_id = ? ', $subject->getEntityId());
        $select->joinLeft(
            ['detail' => $this->resource->getTableName('review_detail')],
            'main_table.review_id = detail.review_id',
            [
                'title',
                'nickname',
                'images',
                'customer_id',
                'detail',
            ]
        );

        $select->where('main_table.status_id = ?', 1);
        $select->where('main_table.review_id > ?', $fromId);
        $select->order('main_table.review_id');
        $select->limit($limit);

        return $this->getConnection()->fetchAssoc($select);
    }

    /**
     * @return \Magento\Framework\DB\Adapter\AdapterInterface
     */
    private function getConnection()
    {
        return $this->resource->getConnection();
    }
}