<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_LabelImage
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2020 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\ParentImage\Model\Rewrite\Indexer\DataProvider\Product;

use Divante\VsbridgeIndexerCatalog\Api\CatalogConfigurationInterface;
use Divante\VsbridgeIndexerCatalog\Api\SlugGeneratorInterface;
use Divante\VsbridgeIndexerCatalog\Model\ProductUrlPathGenerator;
use Divante\VsbridgeIndexerCatalog\Model\ResourceModel\Product\AttributeDataProvider;
use Divante\VsbridgeIndexerCore\Indexer\DataFilter;
use Magento\Store\Model\StoreManagerInterface;
use Divante\VsbridgeIndexerCatalog\Model\ResourceModel\Product;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Divante\VsbridgeIndexerCatalog\Model\Attributes\ProductAttributes;
use Magento\Catalog\Api\ProductRepositoryInterfaceFactory;
/**
 * Class AttributeData
 * @package Aureatelabs\ParentImage\Model\Rewrite\Indexer\DataProvider\Product
 */
class AttributeData extends \Divante\VsbridgeIndexerCatalog\Model\Indexer\DataProvider\Product\AttributeData
{
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Product
     */
    private $product;


    /**
     * AttributeData constructor.
     *
     * @param CatalogConfigurationInterface $configSettings
     * @param SlugGeneratorInterface $slugGenerator
     * @param ProductUrlPathGenerator $productUrlPathGenerator
     * @param DataFilter $dataFilter
     * @param AttributeDataProvider $resourceModel
     * @param StoreManagerInterface $storeManager
     * @param Product $product
     * @param Configurable $configurable
     * @param ProductRepositoryInterfaceFactory $productRepositoryFactory
     */
    public function __construct(
        ProductAttributes $productAttributes,
        CatalogConfigurationInterface $configSettings,
        SlugGeneratorInterface $slugGenerator,
        ProductUrlPathGenerator $productUrlPathGenerator,
        DataFilter $dataFilter,
        AttributeDataProvider $resourceModel,
        StoreManagerInterface $storeManager,
        Product $product,
        Configurable $configurable,
        ProductRepositoryInterfaceFactory $productRepositoryFactory
    ) {

        $this->storeManager = $storeManager;
        $this->product = $product;
        $this->configurable = $configurable;
        $this->_productRepositoryFactory = $productRepositoryFactory;
        parent::__construct(
            $productAttributes,
            $configSettings,
            $slugGenerator,
            $productUrlPathGenerator,
            $dataFilter,
            $resourceModel
        );
    }

    /**
     * @param array $indexData
     * @param int   $storeId
     *
     * @return array
     * @throws \Exception
     */
    public function addData(array $indexData, $storeId)
    {
        $indexData = parent::addData($indexData, $storeId);
        
        foreach ($indexData as $idx => $product) {
      
            if ($product['type_id'] == \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE) {
                $product = $this->_productRepositoryFactory->create()
                    ->getById($product['id']);
                $parent_product_img = $product->getData('image');
                $parent_thumb_image = $product->getData('thumbnail');
                $parent_small_image = $product->getData('small_image');
                $indexData[$idx]['parent_product_img'] = $parent_product_img;
                $indexData[$idx]['parent_thumb_image'] = $parent_thumb_image;
                $indexData[$idx]['parent_small_image'] = $parent_small_image;
            }
         }   
         
        return $indexData;
    }
    /**
     * @param int $childId
     * @return int
     */
    public function getParentProductId($childId)
    {
        $parentConfigObject = $this->configurable->getParentIdsByChild($childId);
        if($parentConfigObject) {
            return $parentConfigObject[0];
        }
        return '';
    }
}
