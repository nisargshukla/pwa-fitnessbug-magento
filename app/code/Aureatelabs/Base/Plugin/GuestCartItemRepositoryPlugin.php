<?php

namespace Aureatelabs\Base\Plugin;

use Magento\Quote\Model\QuoteIdMask;
use Magento\Quote\Model\QuoteIdMaskFactory;
use Magento\Quote\Api\GuestCartManagementInterface;

class GuestCartItemRepositoryPlugin
{
    /**
     * @var \Magento\Quote\Api\CartItemRepositoryInterface
     */
    private $repository;

    /**
     * @var QuoteIdMaskFactory
     */
    private $quoteIdMaskFactory;

    /**
     * @var GuestCartManagementInterface
     */
    private $guestCartManagement;

    /**
     * Constructs a read service object.
     *
     * @param \Magento\Quote\Api\CartItemRepositoryInterface $repository
     * @param QuoteIdMaskFactory $quoteIdMaskFactory
     * @param GuestCartManagementInterface $guestCartManagement
     */
    public function __construct(
        \Magento\Quote\Api\CartItemRepositoryInterface $repository,
        QuoteIdMaskFactory $quoteIdMaskFactory,
        GuestCartManagementInterface $guestCartManagement
    ) {
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->repository = $repository;
        $this->guestCartManagement = $guestCartManagement;
    }

    public function beforeSave(
        \Magento\Quote\Model\GuestCart\GuestCartItemRepository $subject,
        \Magento\Quote\Api\Data\CartItemInterface $cartItem
    ) {
        /** @var $quoteIdMask QuoteIdMask */
        $quoteIdMask = $this->quoteIdMaskFactory->create()->load($cartItem->getQuoteId(), 'masked_id');
        if (!$quoteIdMask->getQuoteId()) {
            $newMaskedId = $this->guestCartManagement->createEmptyCart();
            $quoteIdMask = $this->quoteIdMaskFactory->create()->load($newMaskedId, 'masked_id');
            $quoteIdMask->setMaskedId($cartItem->getQuoteId());
            $quoteIdMask->save();
        }
        return [$cartItem];
    }
}
